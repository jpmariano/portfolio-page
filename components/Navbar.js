import { Navbar } from 'react-bootstrap';

export default function NavBar() {
	return (
		<Navbar className="d-flex justify-content-center">
			<Navbar.Brand href="/" id="brand">
				JPM
			</Navbar.Brand>
		</Navbar>	
	);
};