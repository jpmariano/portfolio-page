import Head from 'next/head';
import PropTypes from 'prop-types';

export default function HeadComp({ headProp }) {
	const { title, description } = headProp;
	
	return (
		<Head>
			<meta charSet="UTF-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0" />
			<title>
				{ title }
			</title>
			<meta name="description" content={ description } />
			<link rel="icon" href="/favicon.png" />
		</Head>
	);
};

Head.propTypes = {
	head: PropTypes.shape({
		title: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired
	})
};