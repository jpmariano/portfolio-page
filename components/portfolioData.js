export const portfolioData = [
	{
		title: 'Budget Tracker Application',
		description: 'A simple budget tracker app wherein the user can visualize their budget trends. This is application uses Node & Express as the back end server. The user\'s data is handled by MongoDB. The UI is created via Next.js which is a framework of React. Other components like Chart.js and Moment.js are used to properly convert data into meaningful information to the user.',
		link: 'https://jp-budget-tracker-app.vercel.app/'
	},
	{
		title: 'Course Booking Application',
		description: 'A fairly easy to use booking app wherein users can either be an admin or not. Regular users can see all the active courses but they can\'t add, delete or edit any of the courses. Only admin users are allowed to do those things. The back end server uses Node & Express and the database is handled by MongoDB. The front end user interface is created via HTML, Bootstrap, CSS and vanilla JavaScript.',
		link: 'https://mariano-jp-portfolio.gitlab.io/capstone-2/client-side'
	},
	{
		title: 'To Do List Application',
		description: 'An application wherein you can jot down all your tasks. Users can classify them as "pending" or "done" depending on their preference. Also, dates are included to further enhance the user experience. The technology stacks used in this application are MongoDB, Node & Express, and Next.js for the UI.',
		link: 'https://to-do-app-frontend.vercel.app/'
	}
];