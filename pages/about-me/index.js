import { Container, Row, Col, Card } from 'react-bootstrap';
import Head from '../../components/Head';

export default function AboutMe() {
	const head = {
		title: 'About Me',
		description: 'Get to know Paolo'
	};

	return (
		<Container className="gen-container text-center">
			<Head headProp={ head } />
			<h2 className="h2-text">About Me</h2>
			<Row>
				<Col className="d-flex justify-content-center align-items-center" md={5}>
					<img
						id="my-face"
						src="/my-face.jpg"
						alt="Paolo's profile picture"
						title="Paolo's profile picture"
					/>
				</Col>
				<Col md={7}>
					<p className="normal-font text-justify">
						I'm a proud Zuitter, I finished Zuitt Coding Bootcamp's program for full stack web development on April 2021. I always had an interest about how websites and mobile applications work. I had first hand experience of it by having my own print on demand business via different online platforms. That's why I took the leap into becoming a web developer.
					</p>
				</Col>
			</Row>
			<Row>
				<Col>
					<p className="normal-font text-justify">
						For my first job, I worked as a freelance web developer (remote) to a small startup called UP-TO-DATE WebDesign from Belgium. I worked closely with their lead developer in creating an application that involves widget creation and rendering. I didn't last long but I really learned a lot about a web developer's working environment there.
					</p>
				</Col>
			</Row>
			<Row>
				<Col>
					<p className="normal-font text-justify">
						Currently, I am an independent contractor for Better Giving. It is an app that connects donors and NPOs to each other. I was hired to do full stack web development work and I love every minute that I work here. Knowing that with my skill set, I'm able to help a cause that keeps on giving to others.
					</p>
				</Col>
			</Row>
			<Row>
				<Col>
					<p className="normal-font text-justify">
						Full stack web development is hard for one individual to do but it's not impossible. I believe that having a specialty within the web development industry is more important. With that said, knowing both sides of the coin really helps in the communication between teammates. I'm more confident to work with back end technology stacks but because I also have experience in working with frontend stacks, it is easy for me to communicate with the front end developers.
					</p>
				</Col>
			</Row>
			
			<Row>
				<Col>
					<h5>TEAM == Together Everyone Achieves More</h5>
				</Col>
			</Row>
			
			<hr className="separator" />
			
			<h4 className="h4-text">Web Design & Development</h4>
			<Row>
				<Col className="card-deck">
          <Card
            className="cards d-flex align-items-center"
            bg="dark"
            text="white"
          >
            <Card.Body>
              <img className="stack-icon" src="/frontend-white.png" alt="" />
              <Card.Title className="card-title text-center">Front End Development</Card.Title>
              <Card.Text className="description-font text-justify">
                Also known as client-side development. The typical tech stack I use are HTML, CSS, Bootstrap, JavaScript, React.js & its framework, Next.js. These languages are used to create a website or web application that users can see and interact with.
              </Card.Text>
            </Card.Body>
          </Card>

          <Card
            className="cards d-flex align-items-center"
            bg="dark"
            text="white"
          >
            <Card.Body>
              <img className="stack-icon" src="/backend-white.png" alt="" />
              <Card.Title className="card-title text-center">Back End Development</Card.Title>
              <Card.Text className="description-font text-justify">
                Also known as server-side development. Back end code handles the request-response of API's for storing and retrieving data. Structuring the data in an efficient way can help with the performance of the site or application. So far, the tech stack that I'm familiar with are Node.js & its framework, Express.js. For database creation, I'm familiar with MongoDB. For cloud services, I'm knowledgeable in Amazon Web Sevices (AWS). I also have an experience working on several blockchains like Ethereum (and other EVM chains) and Cosmos Network just to name a few.
              </Card.Text>
            </Card.Body>
          </Card>

          <Card
            className="cards d-flex align-items-center"
            bg="dark"
            text="white"
          >
            <Card.Body>
              <img className="stack-icon" src="/fullstack-white.png" alt="" />
              <Card.Title className="card-title text-center">Full Stack Development</Card.Title>
              <Card.Text className="description-font text-justify">
                It refers to the development of both front end and back end portions of a website or web application. As a full stack developer, I can work with projects that involve databases, API's and building the user interface of a website or an application.
              </Card.Text>
            </Card.Body>
          </Card>
				</Col>
			</Row>
			
			<hr className="separator" />
			
			<h4 className="h4-text">Contact Info</h4>
			<Row>
				<Col className="d-inline-flex justify-content-center">
					<img
						className="contact-icon"
						src="/email-icon-white.png"
						alt="Paolo's Email Address"
					/>
					<a href="mailto:juanpaolo.mariano28@gmail.com" className="email normal-font text-center contact-text">juanpaolo.mariano28@gmail.com</a>
				</Col>
			</Row>
			<Row>
				<Col className="d-inline-flex justify-content-center">
					<img
						className="contact-icon"
						src="/phone-icon-white.png"
						alt="Paolo's Mobile Number"
					/>
					<p className="normal-font text-center contact-text">(+63) 956 120 7959</p>
				</Col>
			</Row>
			<Row>
				<Col className="d-inline-flex justify-content-center">
					<p>
						<a href="https://github.com/jp-mariano" target="_blank">
							<img
								className="social-icon"
								src="/icon-github.png"
								alt="Paolo's GitHub Profile"
								title="GitHub"
							/>
						</a>
					</p>
					<p>
						<a href="https://gitlab.com/jpmariano" target="_blank">
							<img
								className="social-icon"
								src="/icon-gitlab.png"
								alt="Paolo's GitLab Profile"
								title="GitLab"
							/>
						</a>
					</p>
					<p>
						<a href="https://www.linkedin.com/in/juan-paolo-mariano-16095a166/" target="_blank">
							<img
								className="social-icon"
								src="/icon-linkedin.png"
								alt="Paolo's LinkedIn Profile"
								title="LinkedIn"
							/>
						</a>
					</p>
					<p>
						<a href="https://x.com/1paolomariano" target="_blank">
							<img
								className="social-icon"
								src="/icon-twitterx.png"
								alt="Paolo's X Profile"
								title="X"
							/>
						</a>
					</p>
				</Col>
			</Row>
			
			<div>
				<a href="/portfolio" className="button">Portfolio</a>
			</div>
			<div id="btm-button">
				<a href="/" className="button">Home</a>
			</div>
		</Container>
	);
};