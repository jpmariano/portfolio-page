import { Container } from 'react-bootstrap';
import Head from '../components/Head';

export default function Home() {
	const head = {
		title: 'Welcome to Paolo\'s Web Page!',
		description: 'Full Stack Web Developer'
	};
	
	return (
		<Container id="home-container" className="gen-container d-flex flex-column justify-content-center align-items-center">
			<Head headProp={ head } />
			<h4>Hi, I'm</h4>
			<h1>Paolo</h1>
				<p id="description" className="text-center">
					a humble full stack web developer at your service.
				</p>
				<div>
					<a href="/about-me" className="button">About Me</a>
				</div>
				<div id="btm-button">
					<a href="/portfolio" className="button">Portfolio</a>
				</div>
		</Container>
	);
};