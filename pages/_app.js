import { Fragment } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/global.css';
import Navbar from '../components/Navbar';

export default function MyApp({ Component, pageProps }) {
	return (
		<Fragment>
			<Navbar />
			<Component {...pageProps} />
		</Fragment>
	);
};