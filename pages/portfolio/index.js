import { Container, Row, Col, Card } from 'react-bootstrap';
import Head from '../../components/Head';
import { portfolioData } from '../../components/portfolioData';

export default function Portfolio() {
	const head = {
		title: 'My Portfolio',
		description: 'Paolo\'s portfolio'
	};

	return (
		<Container className="gen-container text-center">
			<Head headProp={ head } />
			<h2 className="h2-text">My Portfolio</h2>
			{ portfolioData.map(element => {
				return (
					<Row key={ element.title }>
						<Col>
              <Card
                className="cards"
                bg="dark"
                text="white"
              >
								<Card.Body>
									<Card.Title className="card-title">
										{ element.title }
									</Card.Title>
									<Card.Text className="description-font text-justify">
										{ element.description }
									</Card.Text>
                  <div>
                    <a href={ element.link } className="button" target="_blank">
                      View
                    </a>
                  </div>
								</Card.Body>
							</Card>
						</Col>
					</Row>
				);
			}) }
			
			<div>
				<a href="/about-me" className="button">About Me</a>
			</div>
			<div id="btm-button">
				<a href="/" className="button">Home</a>
			</div>
		</Container>
	);
};